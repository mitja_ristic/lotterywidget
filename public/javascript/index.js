// Selectors


var formButton      = document.getElementById('submitForm');

var timerHolder     = document.getElementById('timer');

var winnerHolder     = document.getElementById('winnerTicket');

var winnerWrapper    = document.getElementById('winnerCounter');

var listHolder       = document.getElementById('winnersListHolder');


//// Name selectors

var nameInput       = document.getElementById("name");

var nameLabel       = document.getElementById("nameLabel");

var nameBorder      = document.getElementById("nameBorder");

var nameError       = document.getElementById("nameError");


//// Number selectors


var numInput       = document.getElementById("numberInput");

var numLabel       = document.getElementById("numberLabel");

var numBorder      = document.getElementById("numberBorder");

var numError       = document.getElementById("numberError");



// init socket

var socket = io.connect('http://localhost:3000');


// hide/show error msg

function addError(hide, element, holder){
    element =  document.getElementById(element);
    holder =  document.getElementById(holder);
    if (hide){
        element.classList.remove('show');
        holder.classList.remove('error')
    } else{
        element.classList.add('show');
        holder.classList.add('error')
    }
}


// Form animations and validators

//// Name input

nameInput.addEventListener('focus', function (e) {
    nameLabel.classList.add('animate');
    addError(true, 'nameError', 'nameHolder');
    nameBorder.classList.add('animate')
});

nameInput.addEventListener('blur', function (e) {
    nameBorder.classList.remove('animate');
    addError(true, 'nameError', 'nameHolder');
    var nameValue =nameInput.value;
    if (nameValue.length < 1) {
        nameLabel.classList.remove('animate');
    }
});


//// Number input


numInput.addEventListener('focus', function (e) {
    numLabel.classList.add('animate');
    addError(true, 'numberError', 'numberHolder');
    numBorder.classList.add('animate')
});

numInput.addEventListener('blur', function (e) {
    numBorder.classList.remove('animate');
    addError(true, 'numberError', 'numberHolder');
    var numValue =numInput.value;
    if (numValue.length < 1) {
        numLabel.classList.remove('animate');
    }
});


// Form submit with custom validation


//// Validate numbers
function validateNumber(num) {

    if (num === 'none'){
        numError.innerText = 'Enter a valid number';
        addError(false, 'numberError', 'numberHolder');
        return false
    } else{
        if (num <= 0 || num > 30){
            numError.innerText = 'Please enter a value between 1 and 30';
            addError(false, 'numberError', 'numberHolder');
            return false
        }
        if(num > 0 && num <= 30){
            return num
        }
    }
    return false
}

function validateName(){
    var nameValue =nameInput.value;

    if (nameValue.length < 3){
        nameError.innerText = 'Name must be longer then 3 letters';
        addError(false, 'nameError', 'nameHolder');
        return false
    }
    return nameValue
}

////Submit form if valid


formButton.addEventListener('click', function (e) {
    e.preventDefault();
    var data = {};

    var numberValue = undefined;
    if (numInput.value === '0'){
        numberValue = 0
    } else {
        numberValue = parseInt(numInput.value) || 'none';
    }

    var nameToDict = validateName();
    var numToDict = validateNumber(numberValue);

    if (nameToDict && numToDict){
        data['name'] = nameToDict;
        data['number'] = numToDict;
        socket.emit('submitForm', data);

        nameInput.value = '';
        numInput.value = '';
        addError(true, 'nameError', 'nameHolder');
        addError(true, 'numberError', 'numberHolder');
    }


});


//CREATE LIST AND POPULATE IT

function makeUL(array) {
    // Create the list element:
    var list = document.createElement('ul');
    list.setAttribute("id", "winnersList");

    for (var i = 0; i < array.length; i++) {
        // Create the list item:
        var item = document.createElement('li');

        // Set its contents:
        if (array[i].number > 9)
            item.appendChild(document.createTextNode("#" + array[i].number + "....." + array[i].name ));
        else
            item.appendChild(document.createTextNode("#" + array[i].number + "......." + array[i].name ));

        // Add it to the list:
        list.appendChild(item);
    }

    // Finally, return the constructed list:
    return list;
}


//CREATE A LIST ITEM

function makeLI(object) {

    var item = document.createElement('li');

    if (object.number > 9)
        item.appendChild(document.createTextNode("#" + object.number + "....." + object.name ));
    else
        item.appendChild(document.createTextNode("#" + object.number + "......." + object.name ));

    return item;
}


// SOCKET EVENTS

socket.on('timerUpdate', function(count){
    timerHolder.innerText = count;
    if (winnerWrapper.classList.contains('show')){
        winnerWrapper.classList.remove('show')
    }
});


socket.on('gotWinner', function(winner){
    winnerHolder.innerText = "#" + winner.number;
    winnerWrapper.classList.add('show');

    var winnersList = document.getElementById('winnersList');
    winnersList.insertBefore(makeLI(winner), winnersList.firstChild); 
});


socket.on('persistent', function(persistent){
    //in case of server restart
    listHolder.innerHTML = "";
    //fill the list with winners from database
    listHolder.appendChild(makeUL(persistent));
    //console.log(persistent);
});
